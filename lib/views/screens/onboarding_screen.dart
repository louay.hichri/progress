import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress/res/color_manager.dart';
import 'package:progress/res/strings_manager.dart';
import 'package:progress/utils/constants.dart';


class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
          backgroundColor: ColorManager.backgroundColor,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset(
                StringsManager.bubblesShape
              ),
              SizedBox(
                height: size.height * 0.05,
              ),
              Center(
                child: SvgPicture.asset(
                  StringsManager.pictureOnBoarding
                ),
              ),
              SizedBox(height: size.height * 0.05,),
              Center(
                child: Text(
                  StringsManager.onBoardingHeader,
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    fontSize: size.width * 0.04,
                    color: ColorManager.darkColor
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: size.height * 0.03,
                  right: size.width * 0.075,
                  left: size.width * 0.075
                ),
                child: Text(
                  StringsManager.onBoardingSubHeader,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    letterSpacing: 0.7,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    fontSize: size.width * 0.035,
                    color: ColorManager.darkColor
                  ),
                ),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              Center(
                child: SizedBox(
                  width: size.width * 0.75,
                  height: size.height * 0.06,
                  child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              ColorManager.primaryColor),
                          shape: MaterialStateProperty.all<
                              RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      size.width *
                                          0.03)))),

                      onPressed: () {
                        Navigator.pushNamed(context, signUpScreen);
                      },
                      child: Text(
                        StringsManager.onBoardingButtonTitle,
                        style: GoogleFonts.poppins(
                          letterSpacing: 0.7,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                          color: ColorManager.lightColor
                        ),
                      )
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }
}
