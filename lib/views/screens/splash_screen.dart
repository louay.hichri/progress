import 'package:flutter/material.dart';
import 'package:progress/utils/constants.dart';
import 'package:progress/res/color_manager.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Text("splash screen"),
          ElevatedButton(onPressed: () {Navigator.pushNamed(context, onBoardingScreen);}, child: Text("Signin", style: TextStyle(color: ColorManager.primaryColor),)),
        ],
      ),
    ));
  }
}
