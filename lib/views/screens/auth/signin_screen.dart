import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress/res/color_manager.dart';
import 'package:progress/res/strings_manager.dart';
import 'package:progress/services/firebase_service.dart';
import 'package:progress/utils/constants.dart';
import 'package:progress/utils/error_codes.dart';
import 'package:progress/views/widgets/show_dialog.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  FirebaseService _firebaseService = FirebaseService();
  String email = "";
  String password = "";



  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: ColorManager.backgroundColor,
            body: Stack(

              children: [
                SvgPicture.asset(
                  StringsManager.bubblesShape
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: size.height * 0.1
                  ),
                  child: Column(
                    children: [
                      Center(
                        child: Text(
                          StringsManager.signInHeader,
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                              fontSize: size.width * 0.04,
                              color: ColorManager.darkColor
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.015,),
                      Center(
                        child: SvgPicture.asset(
                            StringsManager.pictureSignIn
                        ),
                      ),
                      SizedBox(height: size.height * 0.02,),
                      Center(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.75,
                          child: TextFormField(
                            onChanged: (value) {
                              email = value;
                            },
                            decoration: InputDecoration(
                              hintText: 'Enter your email',
                              hintStyle: GoogleFonts.poppins(
                                  letterSpacing: 0.9,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  fontSize: size.width * 0.03,
                                  color: ColorManager.darkColor
                              ),
                              isDense: true,
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(
                                    size.height * 0.025)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                    size.height * 0.025),
                                borderSide:  BorderSide(
                                  color:  ColorManager.lightColor,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                    size.height * 0.025),
                                borderSide: BorderSide(
                                  color: ColorManager.lightColor,
                                ),
                              ),
                              fillColor: ColorManager.lightColor,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.015,),
                      Center(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.75,
                          child: TextFormField(
                            onChanged: (value) {
                              password = value;
                            },
                            decoration: InputDecoration(
                              hintText: 'Enter password',
                              hintStyle: GoogleFonts.poppins(
                                  letterSpacing: 0.9,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  fontSize: size.width * 0.03,
                                  color: ColorManager.darkColor
                              ),
                              isDense: true,
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(
                                    size.height * 0.025)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                    size.height * 0.025),
                                borderSide:  BorderSide(
                                  color:  ColorManager.lightColor,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                    size.height * 0.025),
                                borderSide: BorderSide(
                                  color: ColorManager.lightColor,
                                ),
                              ),
                              fillColor: ColorManager.lightColor,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.015,),
                      Center(
                        child: InkWell(
                          onTap: ()  {

                            //Navigator.pushNamed(context, resetPasswordScreen);

                          },
                          child: Text(
                            StringsManager.forgetPasswordRedirect,
                            style: GoogleFonts.poppins(
                                letterSpacing: 0.7,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: size.width * 0.035,
                                color: ColorManager.primaryColor
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.015,),
                      Center(
                        child: SizedBox(
                          width: size.width * 0.75,
                          height: size.height * 0.06,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      ColorManager.primaryColor),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              size.width *
                                                  0.03)))),

                              onPressed: () async {
                                try {
                                  await _firebaseService.signIn(email, password);
                                  Navigator.pushNamed(context, homeScreen);
                                } catch(e) {
                                  dialog(context,e.toString());
                                }
                              },
                              child: Text(
                                StringsManager.signInButtonTitle,
                                style: GoogleFonts.poppins(
                                    letterSpacing: 0.7,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                    color: ColorManager.lightColor
                                ),
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.015,),
                      Row(children: <Widget>[
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: size.width * 0.15, right: size.width * 0.035
                              ),
                              child: Divider(
                                color: ColorManager.darkColor,
                                height: 36,
                              )),
                        ),
                        Text(
                            "OR",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                              fontSize: size.width * 0.04,
                              color: ColorManager.darkColor
                          ),
                        ),
                        Expanded(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: size.width * 0.035,
                                  right: size.width * 0.15
                              ),
                              child: const Divider(
                                color: Colors.black,
                                height: 36,
                              )),
                        ),
                      ]),


                      SizedBox(height: size.height * 0.015,),
                      Center(
                        child: SizedBox(
                          width: size.width * 0.75,
                          height: size.height * 0.06,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      ColorManager.lightColor),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              size.width *
                                                  0.03)))),

                              onPressed: ()  async {
                                  await _firebaseService.signInWithGoogle();
                              },
                              child: Row(
                                children: [
                                  SizedBox(width: size.width * 0.035,),
                                  SvgPicture.asset(
                                    StringsManager.googleLogo,
                                    width: size.width * 0.05,
                                  ),
                                  SizedBox(width: size.width * 0.05,),
                                  Text(
                                    StringsManager.signInGoogleButtonTitle,
                                    style: GoogleFonts.poppins(
                                        letterSpacing: 0.7,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal,
                                        color: ColorManager.darkColor
                                    ),
                                  ),
                                ],
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.02,),
                      Center(
                        child: SizedBox(
                          width: size.width * 0.75,
                          height: size.height * 0.06,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      ColorManager.primaryColor),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              size.width *
                                                  0.03)))),

                              onPressed: () {

                              },
                              child: Row(
                                children: [
                                  SizedBox(width: size.width * 0.035,),
                                  SvgPicture.asset(
                                    StringsManager.phoneLogo,
                                    color: ColorManager.lightColor,
                                    width: size.width * 0.05,
                                  ),
                                  SizedBox(width: size.width * 0.05,),
                                  Text(
                                    StringsManager.signInPhoneButtonTitle,
                                    style: GoogleFonts.poppins(
                                        letterSpacing: 0.7,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal,
                                        color: ColorManager.lightColor
                                    ),
                                  ),
                                ],
                              )
                          ),
                        ),
                      ),

                      SizedBox(height: size.height * 0.05,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            StringsManager.signInNoAccount,
                            style: GoogleFonts.poppins(
                                color: ColorManager.darkColor
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, signUpScreen);
                            },
                            child: Text(
                              StringsManager.signUp,
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  color: ColorManager.primaryColor
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),

              ],
            )
        )
    );
  }
}
