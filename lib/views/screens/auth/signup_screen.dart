import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress/res/color_manager.dart';
import 'package:progress/res/strings_manager.dart';
import 'package:progress/utils/constants.dart';


class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
          backgroundColor: ColorManager.backgroundColor,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset(
                StringsManager.bubblesShape
              ),
              SizedBox(height: size.height * 0.025,),
              Center(
                child: Text(
                  StringsManager.signUpHeader,
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      fontSize: size.width * 0.04,
                      color: ColorManager.darkColor
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.015,),
              Center(
                child: Text(
                  StringsManager.signUpSubHeader,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                      letterSpacing: 0.7,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: size.width * 0.035,
                      color: ColorManager.darkColor
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.035,),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: TextFormField(
                    onChanged: (value) {

                    },
                    decoration: InputDecoration(
                      hintText: 'Enter your full name',
                      hintStyle: GoogleFonts.poppins(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: size.width * 0.03,
                          color: ColorManager.darkColor
                      ),
                      isDense: true,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(
                            size.height * 0.025)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide:  BorderSide(
                          color:  ColorManager.lightColor,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide: BorderSide(
                          color: ColorManager.lightColor,
                        ),
                      ),
                      fillColor: ColorManager.lightColor,
                    ),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02,),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: TextFormField(
                    onChanged: (value) {

                    },
                    decoration: InputDecoration(
                      hintText: 'Enter your email',
                      hintStyle: GoogleFonts.poppins(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: size.width * 0.03,
                          color: ColorManager.darkColor
                      ),
                      isDense: true,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(
                            size.height * 0.025)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide:  BorderSide(
                          color:  ColorManager.lightColor,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide: BorderSide(
                          color: ColorManager.lightColor,
                        ),
                      ),
                      fillColor: ColorManager.lightColor,
                    ),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02,),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: TextFormField(
                    onChanged: (value) {

                    },
                    decoration: InputDecoration(
                      hintText: 'Enter password',
                      hintStyle: GoogleFonts.poppins(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: size.width * 0.03,
                          color: ColorManager.darkColor
                      ),
                      isDense: true,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(
                            size.height * 0.025)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide:  BorderSide(
                          color:  ColorManager.lightColor,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide: BorderSide(
                          color: ColorManager.lightColor,
                        ),
                      ),
                      fillColor: ColorManager.lightColor,
                    ),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02,),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: TextFormField(
                    onChanged: (value) {

                    },
                    decoration: InputDecoration(
                      hintText: 'Confirm password',
                      hintStyle: GoogleFonts.poppins(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: size.width * 0.03,
                          color: ColorManager.darkColor
                      ),
                      isDense: true,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(
                            size.height * 0.025)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide:  BorderSide(
                          color:  ColorManager.lightColor,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            size.height * 0.025),
                        borderSide: BorderSide(
                          color: ColorManager.lightColor,
                        ),
                      ),
                      fillColor: ColorManager.lightColor,
                    ),
                  ),
                ),
              ),

              SizedBox(height: size.height * 0.05,),


              Center(
                child: SizedBox(
                  width: size.width * 0.75,
                  height: size.height * 0.06,
                  child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              ColorManager.primaryColor),
                          shape: MaterialStateProperty.all<
                              RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      size.width *
                                          0.03)))),

                      onPressed: () {

                      },
                      child: Text(
                        StringsManager.signUpButtonTitle,
                        style: GoogleFonts.poppins(
                            letterSpacing: 0.7,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            color: ColorManager.lightColor
                        ),
                      )
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.025,),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    StringsManager.signUpAlreadyHaveAccount,
                    style: GoogleFonts.poppins(
                        color: ColorManager.darkColor
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, signInScreen);
                    },
                    child: Text(
                      StringsManager.signIn,
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        color: ColorManager.primaryColor
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        )
    );
  }
}
