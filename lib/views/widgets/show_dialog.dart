import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress/res/color_manager.dart';
import 'package:progress/res/strings_manager.dart';

dialog(BuildContext context, String Message) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        final size = MediaQuery.of(context).size;
        return AlertDialog(
          backgroundColor: ColorManager.backgroundColor,
          insetPadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                  Radius.circular(
                    size.width * 0.05
                  )
              )
          ),

          title: Row(
            children: [
              Text(
                  "Oops :(",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    fontSize: size.width * 0.04,
                    color: ColorManager.darkColor
                ),
              ),
              const Spacer(),
              IconButton(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                hoverColor: Colors.transparent,
                focusColor: Colors.transparent,
                icon: Icon(
                    Icons.close,
                    color: ColorManager.darkColor,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
          content: SizedBox(
            height: size.height * 0.35,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[
                SvgPicture.asset(
                  StringsManager.errorIcon,
                  height: size.height * 0.15,
                  width: size.width * 0.8,
                ),
                Text(
                  Message,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                      letterSpacing: 0.7,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: size.width * 0.035,
                      color: ColorManager.darkColor
                  ),
                )
              ],
            ),
          ),
        );
      }
  );
}