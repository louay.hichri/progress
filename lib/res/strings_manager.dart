class StringsManager {
  static const String onBoardingHeader = "Gets things done with PROGRESS";
  static const String onBoardingSubHeader = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum dictum tempus, interdum at dignissim metus. Ultricies sed nunc.";
  static const String onBoardingButtonTitle = "Get Started";
  static const String pictureOnBoarding = "assets/images/picture-on-boarding.svg";
  static const String bubblesShape = "assets/images/bubbles-shape.svg";
  static const String signIn = "Sign In";


  static const String signUpHeader = "Welcome OnBoard!";
  static const String signUpSubHeader = "Let’s help you meet up your tasks";
  static const String signUpButtonTitle = "Register";
  static const String signUpAlreadyHaveAccount = "Already have an account ?  ";

  static const String pictureSignIn = "assets/images/picture-signin-screen.svg";
  static const String signInHeader = "Welcome Back!";
  static const String forgetPasswordRedirect = "Forgot Password";
  static const String signInButtonTitle = "Sign In";
  static const String signInNoAccount = "Don’t have an account ?  ";
  static const String signUp = "Sign Up";
  static const String googleLogo = "assets/images/google.svg";
  static const String signInGoogleButtonTitle = "Sign in with Google";
  static const String signInPhoneButtonTitle = "Sign in with Phone";
  static const String phoneLogo = "assets/images/phone.svg";
  static const String errorIcon = "assets/images/error-icon.svg";
}