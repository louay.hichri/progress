import 'package:flutter/material.dart';

class ColorManager {
  static Color backgroundColor = const Color(0XFFE5E5E5);
  static Color primaryColor = const Color(0XFF50C2C9);
  static Color lightColor = const Color(0XFFFFFFFF);
  static Color darkColor = const Color(0XFF000000);
}