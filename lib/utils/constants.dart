const String splashScreen = "/";
const String onBoardingScreen = "/on-boarding";
const String signInScreen = "/sign-in";
const String signUpScreen = "/sign-up";
const String resetPasswordScreen = "/reset-password";
const String homeScreen = "/home";