const signInErrorCodes = <String, String> {
  "user-not-found": "User not found",
  "wrong-password": "Wrong password",
  "user-disabled": "User disabled"
};
