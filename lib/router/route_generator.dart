import 'package:flutter/material.dart';
import 'package:progress/views/screens/home/home_screen.dart';
import 'package:progress/views/screens/onboarding_screen.dart';
import 'package:progress/views/screens/auth/reset_password_screen.dart';
import 'package:progress/views/screens/auth/signin_screen.dart';
import 'package:progress/views/screens/auth/signup_screen.dart';
import 'package:progress/views/screens/splash_screen.dart';

import '../utils/constants.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch(settings.name) {
      case splashScreen:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      case onBoardingScreen:
        return MaterialPageRoute(builder: (_) => const OnBoarding());
      case signInScreen:
        return MaterialPageRoute(builder: (_) => const SignIn());
      case signUpScreen:
        return MaterialPageRoute(builder: (_) => const SignUp());
      case resetPasswordScreen:
        return MaterialPageRoute(builder: (_) => const ResetPassword());
      case homeScreen:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      default:
        return MaterialPageRoute(builder: (_) => Scaffold(
          body: Center(
            child: Text("No route defined with this ${settings.name}"),
          ),
        ));
    }
  }
}